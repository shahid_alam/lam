# --------------------------------------------------------------------------------------------------------------
#
# Filename: CallSequences.py
# Author: Shahid Alam (shalam3@gmail.com)
# Dated: February 4, 2022
#
# --------------------------------------------------------------------------------------------------------------

from __future__ import print_function
import sys, os, argparse, math, numpy as np, pandas as pd
from treelib import Node, Tree

from Classify import *

__DEBUG__ = False
__PRINT_SEQUENCE__ = False
__GENERATE_CALLGRAPHS__ = False
__CLASSIFY__ = True

'''
Here is one example of the API call sequences from the dt_string:

Children of <android.app.Dialog$1: void run()>:
       <android.app.Dialog: void access$000(android.app.Dialog)>
Children of <android.app.Dialog: void access$000(android.app.Dialog)>:
       <android.app.Dialog: void dismissDialog()>
Children of <android.app.Dialog: void dismissDialog()>:
       <android.app.Dialog: void sendDismissMessage()>
       <android.app.SearchDialog: void onStop()>
       <android.app.Dialog: void onStop()>
Children of <android.app.Dialog: void sendDismissMessage()>:
Children of <android.app.SearchDialog: void onStop()>:
       <android.content.ContextWrapper: void unregisterReceiver(android.content.BroadcastReceiver)>
Children of <android.app.Dialog: void onStop()>:

The dominance tree (OR one kind of sequences of API calls) generated will be:
<android.app.Dialog$1: void run()>:                                 (1)
    <android.app.Dialog: void access$000(android.app.Dialog)>:      (2)
        <android.app.Dialog: void dismissDialog()>:                 (3)
            <android.app.Dialog: void sendDismissMessage()>         (4)
            <android.app.SearchDialog: void onStop()>:              (5)
                <android.content.ContextWrapper: void unregisterReceiver(android.content.BroadcastReceiver)>       (7)
            <android.app.Dialog: void onStop()>                     (6)

The DT will look like:

                           (1)

                            |

                           (2)

                            |

                           (3)

                   /        |        \

                 (4)       (5)       (6)

                            |

                           (7)

The above DT has 3 different Call sequences.
'''
class APICallMap():
	def __init__(self):
		self.API_CALL_SEQS_MAP_FILES = dict()
		self.FILES = dict()
		self.FILES_LIMIT = 7     # Only keep, if the sequence is present in more than 7 files/samples
		self.COMMON_LIMIT = 0.25 # Keep the feature if its only common by 25% in both classes
		self.LIMIT_STR = 1048576 # 2 to the power 20 = 1 MB

	def add(self, seq_name, filename):
		self.FILES[filename] = list()
		if (seq_name in self.API_CALL_SEQS_MAP_FILES.keys()):
			files = self.API_CALL_SEQS_MAP_FILES[seq_name]
			files[filename] = 1
		else:
			files = dict()
			files[filename] = 1
			self.API_CALL_SEQS_MAP_FILES[seq_name] = files

	def print(self):
		for seq in self.API_CALL_SEQS_MAP_FILES.keys():
			print(seq, self.API_CALL_SEQS_MAP_FILES[seq])

	def save_csv(self, csv_filename):
		# Remove the sequences that are found in < self.FILES_LIMIT (7) files/samples
		LAM = dict()
		print("Number of raw features extracted: %d"%len(self.API_CALL_SEQS_MAP_FILES))
		for seq in self.API_CALL_SEQS_MAP_FILES.keys():
			files = self.API_CALL_SEQS_MAP_FILES[seq]
			if len(files) >= self.FILES_LIMIT:
				m = 0
				b = 0
				for filename in files:
					if ('__m__' in filename):
						m += 1
					elif ('__b__' in filename):
						b += 1
				# Some feature (sequence) selection performed here
				# m == b; Feature is present in the same quantity both in malware and benign
				# m > b; Feature is present in more quantity in malware than benign
				# m < b; Feature is present in less quantity in malware than benign
				# We only keep those features which are common in
				# both malware and bengin by <= self.COMMON_LIMIT (25%)
				common = 100
				if m < b:
					common = common * (m / b)
				elif m > b:
					common = common * (b / m)
				if common <= self.COMMON_LIMIT:
					LAM[seq] = files
		self.API_CALL_SEQS_MAP_FILES = None

		seqs = LAM.keys()
		for filename in self.FILES:
			array = [0] * len(seqs)
			self.FILES[filename] = array

		seq_no = 0
		str = ''
		with open(csv_filename, 'w') as f:
			f.write("%s"%str)
		f.close()
		with open(csv_filename, 'a') as f:
			for seq in LAM.keys():
				files = LAM[seq]
				for filename in files:
					times = files[filename]
					array = self.FILES[filename]
					array[seq_no] = times
				str += 'S' + seq_no.__str__() + ','
				if __PRINT_SEQUENCE__:
					print('S' + seq_no.__str__() + ':' + seq)
				seq_no += 1
				if len(str) >= self.LIMIT_STR:
					print("Writing to file %s => %d bytes"%(csv_filename, len(str)))
					f.write("%s"%str)
					str = ''

			str += 'Class' + '\n'
			print("Writing to file %s => %d bytes"%(csv_filename, len(str)))
			f.write("%s"%str)

			str = ''
			for filename in self.FILES.keys():
				type = 'Class'
				if ('__m__' in filename):
					type = 1
				elif ('__b__' in filename):
					type = 0
				seq_api_times = self.FILES[filename]
				for time in seq_api_times:
					str += '' + time.__str__() + ','
				str += type.__str__() + '\n'
				if len(str) >= self.LIMIT_STR:
					print("Writing to file %s => %d bytes"%(csv_filename, len(str)))
					f.write("%s"%str)
					str = ''
			print("Writing to file %s => %d bytes"%(csv_filename, len(str)))
			f.write("%s"%str)
		f.close()

class DTSCallSequences():

	def __init__(self):
		self.APICallMap = APICallMap()

	#
	# Build the dominance tree of API calls in dt_string
	# Format of a simple dt_string return by DroidASAT is given above
	# 
	def BuildDTSTree(self, dt_string):
		dts_tree = None
		DT_START = "DT START"
		DT_END = "DT END"
		PARENT_STRING = "Children of"
		#
		# Process the dt string and store the dt patterns in a list
		#
		DONE = 0
		lines = dt_string.split('\n')
		nlines = len(lines)
		count = 0
		while count < nlines:
			line = lines[count].strip()
			if (DONE == 1 or DT_END in line):
				break
			if (DT_START in line):
				dominator_node = ""
				root_node = "DTS_TREE"
				dts_tree = Tree()
				dts_tree.create_node(root_node, root_node)
				while count < nlines:
					line = lines[count].strip()
					if len(line) > 2:
						if (DONE == 1 or DT_END in line):
							break
						elif (PARENT_STRING in line):
							dominator_node = str(line[line.find("<"):line.find(">")+1])
							if dts_tree.get_node(dominator_node) == None:
								if __DEBUG__:
									print("D: " + dominator_node)
								dts_tree.create_node(dominator_node, dominator_node, parent=root_node)
						elif dts_tree.get_node(dominator_node) != None:
							child_node = str(line[line.find("<"):line.find(">")+1])
							if __DEBUG__:
								print("   C: " + child_node)
							if dts_tree.get_node(child_node) == None:
								dts_tree.create_node(child_node, child_node, parent=dominator_node)
							else:
								# Move only if the dominator_node is not already the ancestor of the child_node
								# and child_node is not itself the dominator node
								if dts_tree.is_ancestor(dominator_node, child_node) == False:
									if (child_node != dominator_node):
										dts_tree.move_node(child_node, dominator_node)
									elif __DEBUG__:
										print("%s (dominator node) is the %s (child node)\n"%(dominator_node, child_node))
								elif __DEBUG__:
									print("%s is already the ancestor of %s\n"%(dominator_node, child_node))
					count += 1
			else:
				count += 1

		return dts_tree

	#
	# Find and return a list of API call sequences in the dts_tree
	# Each API call sequences is unique
	#
	def BuildCallSequences(self, dts_tree):
		call_seqs = list()

		# Find all the paths (API call sequences)
		# in the dts_tree and append them to a list
		if dts_tree is not None:
			paths = dts_tree.paths_to_leaves()
			for path in paths:
				path = str(path)
				call_seqs.append(path)

		return call_seqs

	#
	# A list of API call sequences is input to this function
	# Go through each API call sequence (path) and
	# assign weight according to the number of times
	# it is repeated in the list of API call sequences
	#
	def AssignWeight(self, call_seqs):
		call_seqs_with_weight = dict()

		for seq in call_seqs:
			for path in seq:
				if path in call_seqs_with_weight:
					call_seqs_with_weight[path] += 1
				else:
					call_seqs_with_weight[path] = 1

		return call_seqs_with_weight

	#
	# Take sequences that are common both in benign
	# and malware out from the malware sequences
	# Then return the malware sequences
	# These weighted sequences can be used to classify malware
	#
	def TakeCommonOut(self, call_seqs_malware, call_seqs_benign):
		call_seqs_with_weight = dict()

		for seq_m in call_seqs_malware:
			if seq_m in call_seqs_benign:
				continue
			else:
				weight = call_seqs_malware[seq_m]
				call_seqs_with_weight[seq_m] = weight

		return call_seqs_with_weight

	#
	# A list of API call sequences is input to this function
	# Go through each API call sequence (path) and
	# assign weight according to the number of times
	# it is repeated in the list of API call sequences
	#
	def UpdateAPICallMap(self, call_seqs, filename):
		for seq in call_seqs:
			self.APICallMap.add(seq, filename)

	def PrintAPICallMap(self):
		self.APICallMap.print()

	def SaveAPICallMapAsCSV(self, filename):
		self.APICallMap.save_csv(filename)


def GenerateCallGraphFiles(list_of_files, output_dir, is_malware):
	command_to_run_DroidASAT = 'java -Xms512m -Xmx1024m -cp ".;DroidASAT.jar;./lib/rt.jar;./lib/sootclasses-trunk-jar-with-dependencies.jar;./lib/soot-infoflow.jar;./lib/soot-infoflow-android.jar" DroidASAT.main "./lib/rt.jar" "./android-jar"'
	temp_dir = './temp/'

	list_of_files = os.path.abspath(list_of_files)
	file = open(list_of_files, 'r')
	files_apk = file.read()
	file.close()

	for f in files_apk.split('\n'):
		f = f.strip()
		if (f != ""):
			full_f_path = os.path.abspath(f)
			if not os.path.isfile(full_f_path):
				print ("Error:Reading file: APK file %s does not exist"%full_f_path)
			else:
				if is_malware:
					full_outfilepath = os.path.join(output_dir, os.path.basename(full_f_path)) + '.__m__.txt'
				else:
					full_outfilepath = os.path.join(output_dir, os.path.basename(full_f_path)) + '.__b__.txt'
				#command = 'java -Xms512m -Xmx1024m -cp ".;DroidASAT.jar;./lib/rt.jar;./lib/sootclasses-trunk-jar-with-dependencies.jar;./lib/soot-infoflow.jar;./lib/soot-infoflow-android.jar" DroidASAT.main "./lib/rt.jar" "./android-jar" "air.wowtest-1000000.apk" "./temp/" > callgraph.txt'
				command = command_to_run_DroidASAT + ' "' + full_f_path + '" "' + temp_dir + '" > "' + full_outfilepath + '"'
				print(command + "\n")
				if __DEBUG__:
					print("GenerateCallGraphFiles: Executing command: %s"%command)
					sys.stdout.flush()
				result = os.system(command)
				if result != 0:
					print("Error:GenerateCallGraphFiles: Running command: %s"%command)


# Generate call graphs from APKs
def GenerateCallGraphsFromAPK(output_dir, list_of_malware_files, list_of_benign_files):
	output_dir = os.path.abspath(output_dir)
	is_malware = True
	GenerateCallGraphFiles(list_of_malware_files, output_dir, is_malware)
	is_malware = False
	GenerateCallGraphFiles(list_of_benign_files, output_dir, is_malware)

# Feature Extraction
def FeatureExtraction(list_of_files, csv_filename):
	DT = DTSCallSequences()

	list_of_files = list_of_files.strip()
	list_of_files = os.path.abspath(list_of_files)
	file = open(list_of_files, 'r')
	files_callgraph = file.read()
	file.close()

	for f in files_callgraph.split('\n'):
		f = f.strip()
		if (f != ""):
			full_f_path = os.path.abspath(f)
			if not os.path.isfile(full_f_path):
				print ("Error:Reading file: Call graphs file %s does not exist"%full_f_path)
			else:
				print ("Processing file %s"%full_f_path)
				file = open(full_f_path, 'r')
				dt_string = file.read()
				file.close()
				dts_tree = DT.BuildDTSTree(dt_string)
				call_seqs = DT.BuildCallSequences(dts_tree)
				filename = os.path.basename(full_f_path)
				DT.UpdateAPICallMap(call_seqs, filename)
				if __DEBUG__:
					DT.PrintAPICallMap()
	DT.SaveAPICallMapAsCSV(csv_filename)

def RunClassifier(RFE, CORR, dataset, dataset_filename):
	CL = Classifier()
	CL.classify(RFE, CORR, dataset, dataset_filename)


if __name__=="__main__":
	if __GENERATE_CALLGRAPHS__:
		# It needs the malware and bengn APK files to generate the callgraphs
		# The repository does not contain these APK files, because
		# we have already generated the callgraphs in the DIR ./callgraphs
		# and they can be used for classification
		output_dir = "callgraphs"
		list_of_malware_files = "malware_files.txt"
		list_of_benign_files = "benign_files.txt"
		GenerateCallGraphsFromAPK(output_dir, list_of_malware_files, list_of_benign_files)
	elif __CLASSIFY__:
		# It uses the callgraphs already generated by the above code
		parser = argparse.ArgumentParser()
		parser.add_argument("-f", type=str)
		parser.add_argument("-csv", type=str)
		args = parser.parse_args()
		list_of_files = args.f
		csv_filename = args.csv

		def PrintUsage():
			print ("Usage:\npython CallSequences.py -f <list_of_files> -csv <csv_filename>")

		if list_of_files is None:
			print ("Please enter the name of the file containing the list of callgraph files")
			PrintUsage()
			sys.exit(1)

		print ("list_of_files: %s"%list_of_files)
		print ("csv_filename: %s"%csv_filename)

		FeatureExtraction(list_of_files, csv_filename)
		dataset = pd.read_csv(csv_filename)
		RFE = True  # Recursive feature elimination
		CORR = False   # correlation (Pearson / ChiSquare)
		RunClassifier(RFE, CORR, dataset, csv_filename)
