This is the repository created for storing the code and data to test LAM (Leading API-call Map).
LAM is a new technique that helps detect suspicious call sequences in an application.

We do not want to store the real malware programs in this public repository, therefore,
the repository does not contain the actual malware and benign files but only their callgraphs.
These callgraphs are then used to build the LAM for feature extraction, selection and classification.

More details on LAM are available in the paper submitted to 'The Computer Journal'  for publication.