# --------------------------------------------------------------------------------------------------------------
#
# Filename: Classify.py
# Author: Shahid Alam (shalam3@gmail.com)
# Dated: February, 23, 2022
#
# --------------------------------------------------------------------------------------------------------------

import warnings
import numpy as np
from scipy import interp
import matplotlib.pyplot as plt

from sklearn.metrics import classification_report, confusion_matrix, accuracy_score, roc_curve, auc, roc_auc_score
from sklearn.model_selection import StratifiedKFold, cross_val_score, train_test_split

from sklearn.exceptions import UndefinedMetricWarning
from sklearn.naive_bayes import GaussianNB
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.svm import LinearSVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier

__ROC_CURVE__ = True
__DEBUG__  = True

#
#
#
class Classifier:
	def __init__(self):
		self.CLASS_NAME = "Class"
		self.n_classes = 0

	#
	#
	#
	def classify(self, RFE, CORR, dataset, dataset_filename, NFold=2, testSize=20):
		y = dataset.Class                           # Labels
		y = np.array(y)
		X = dataset.drop(self.CLASS_NAME, axis=1)   # Features
		X = np.array(X)

		if RFE:
			if __DEBUG__:
				print("Applying RFE to further improve feature selection using linear SVM")
				print("Number of Features BEFORE: " + str(len(dataset.columns)))
			from sklearn.feature_selection import RFE
			svm = LinearSVC(max_iter=3000)
			# create the RFECV model for the classifier and select attributes
			rfe = RFE(svm, verbose=1)   # RFE using SVM
			X = rfe.fit_transform(X, y)
			if __DEBUG__:
				print("Number of Features AFTER: " + str(len(X[0])))

		if CORR:
			if __DEBUG__:
				print("Applying correlation (Pearson / ChiSquare correlation) to further improve feature selection")
				print("Number of Features BEFORE: " + str(len(dataset.columns)))
			from sklearn.feature_selection import SelectKBest, f_regression as pearson_corr, chi2
			# define feature selection
			n = int(len(dataset.columns) / 2)  # 50% of the original features
			if n <= 0:
				n = 1
			corr = SelectKBest(chi2, n)
			# apply feature selection
			X = corr.fit_transform(X, y)
			if __DEBUG__:
				print("Number of Features AFTER: " + str(len(X[0])))

		names = [
				"Nearest Neighbors",
				"Naive Bayes",
				"Random Forest",
				"Decision Tree",
				"AdaBoost",
				"Linear SVM",
				"Neural Networks"
				]
		classifiers = [
						KNeighborsClassifier(n_neighbors=3),
						GaussianNB(),
						RandomForestClassifier(max_depth=5),
						DecisionTreeClassifier(max_depth=5),
						AdaBoostClassifier(),
						LinearSVC(max_iter=3000),
						# Neural Networks
						# solver: The solver for weight optimization
						# lbfgs - is an optimizer in the family of quasi-Newton methods
						# works good with smaller datasets
						MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(5, 2), random_state=0)
		]

		# It's not an n-fold cross validation but a split
		if NFold < 2:
			ts = testSize
			testSize = testSize / 100
			train_X, test_X, train_y, test_y = train_test_split(X, y, test_size=testSize, random_state=0)

			n = 0
			filename_result = dataset_filename + "_" + str(ts) + ".results.txt"
			file_result = open(filename_result, "w")
			print("Classifying and saving results in " + filename_result)
			while n < len(names):
				clf = classifiers[n]
				name = names[n]
				cn = "Classifying with " + name + " . . . . .\n"
				filename_roc = dataset_filename + "_" + name + "_" + str(ts) + ".png"
				self.classify_with_split(cn, train_X, test_X, train_y, test_y, clf, file_result, filename_roc)
				n += 1
			file_result.close()
		# 10-fold cross validation
		else:
			N = 10
			cv = StratifiedKFold(n_splits=N)
			train_X = []
			test_X = []
			train_y = []
			test_y = []
			for train_i, test_i in cv.split(X, y):
				train_X.append(X[train_i])
				test_X.append(X[test_i])
				train_y.append(y[train_i])
				test_y.append(y[test_i])

			n = 0
			filename_result = dataset_filename + "_" + str(N) + "-fold.results.txt"
			file_result = open(filename_result, "w")
			print("Classifying and saving results in " + filename_result)
			while n < len(names):
				clf = classifiers[n]
				name = names[n]
				cn = "Classifying with " + name + " . . . . .\n"
				filename_roc = dataset_filename + "_" + name + "_" + str(N) + "-fold.png"
				self.classify_with_N_fold(cn, train_X, test_X, train_y, test_y, clf, file_result, filename_roc)
				n += 1
			file_result.close()

	#
	# It's an n-fold cross validation
	# cv is the n-fold data splitter
	# clf is the classifier passed
	# e.g., NaiveBayes, RandomForest etc
	#
	def classify_with_N_fold(self, cn, train_X, test_X, train_y, test_y, clf, file_result, filename_roc):
		try:
			result = cn
			warnings.filterwarnings("ignore", category=UndefinedMetricWarning)

			tprs = []
			aucs = []
			roc_fpr = np.linspace(0, 1, 100)
			mean_tpr = 0
			mean_fpr = 0
			mean_accuracy = 0
			mean_auc = 0
			folds = len(train_X)
			fold = folds - 1
			while fold >= 0:
				clf.fit(train_X[fold], train_y[fold])
				predicted = clf.predict(test_X[fold])
				expected = test_y[fold]
				#report = classification_report(predicted, expected)
				accuracy = accuracy_score(predicted, expected, normalize=True)
				mean_accuracy += accuracy
				cm = confusion_matrix(predicted, expected)
				tn, fp, fn, tp = cm.ravel()
				if tp != 0 or fn != 0:         # check divide by zero
					mean_tpr += tp / (tp + fn)
				if fp != 0 or tn != 0:         # check divide by zero
					mean_fpr += fp / (fp + tn)
				fpr, tpr, thresholds = roc_curve(test_y[fold], predicted)

				interp_tpr = interp(roc_fpr, fpr, tpr)
				interp_tpr[0] = 0.0
				tprs.append(interp_tpr)

				mean_auc += auc(fpr, tpr)

				fold -= 1

			roc_tpr = np.mean(tprs, axis=0)
			roc_tpr[-1] = 1.0

			mean_tpr = mean_tpr/folds
			mean_fpr = mean_fpr/folds
			mean_accuracy = mean_accuracy/folds
			mean_auc = mean_auc/folds
			#result += "Classification Report:\n"
			#result += str(report)
			result += "TPR = " + str(mean_tpr) + "\n"
			result += "FPR = " + str(mean_fpr) + "\n"
			result += "Accuracy = " + str(mean_accuracy) + "\n"
			result += "AUC = " + str(mean_auc) + "\n"
			#result += "Confusion matrix:\n" + str(cm) + "\n"
			file_result.write(result)
			if __DEBUG__:
				print(result)

			if __ROC_CURVE__:
				# Plotting the ROC curve
				fig = plt.figure()
				plt.plot(roc_fpr, roc_tpr, color="darkblue", label="AUC = "+str(mean_auc))
				plt.legend(loc=4)
				plt.title("ROC curve plot", fontsize=14, verticalalignment="top", horizontalalignment="center")
				plt.xlabel("False positive rate", labelpad=3, fontsize=12)
				plt.ylabel("True positive rate", labelpad=3, fontsize=12)
				#plt.show()
				print("Saving the ROC curve: " + filename_roc)
				fig.savefig(filename_roc)
		except ValueError:
			print("--- Data ERROR ---")
			print("Error:Classify::classify_with_split: Value Error!")
			pass

	#
	# It's not an n-fold cross validation but a %age split,
	# e.g., 80% training and 20% testing
	# clf is the classifier passed
	# e.g., NaiveBayes, RandomForest etc
	#
	def classify_with_split(self, cn, train_X, test_X, train_y, test_y, clf, file_result, filename_roc):
		try:
			result = cn
			print(result)
			warnings.filterwarnings("ignore", category=UndefinedMetricWarning)

			clf.fit(train_X, train_y)
			predicted = clf.predict(test_X)
			expected = test_y

			tpr = 0
			fpr = 0
			#report = classification_report(predicted, expected)
			accuracy = accuracy_score(predicted, expected, normalize=True)
			cm = confusion_matrix(predicted, expected)
			tn, fp, fn, tp = cm.ravel()
			if tp != 0 or fn != 0:         # check divide by zero
				tpr = tp / (tp + fn)
			if fp != 0 or tn != 0:         # check divide by zero
				fpr = fp / (fp + tn)
			roc_fpr, roc_tpr, thresholds = roc_curve(test_y, predicted)
			roc_auc = auc(roc_fpr, roc_tpr)

			#result += "Classification Report:\n"
			#result += str(report)
			result += "TPR = " + str(tpr) + "\n"
			result += "FPR = " + str(fpr) + "\n"
			result += "Accuracy = " + str(accuracy) + "\n"
			result += "AUC = " + str(roc_auc) + "\n"
			#result += "Confusion matrix:\n" + str(cm) + "\n"
			file_result.write(result)
			if __DEBUG__:
				print(result)

			if __ROC_CURVE__:
				# Plotting the ROC curve
				fig = plt.figure()
				plt.plot(roc_fpr, roc_tpr, color="darkblue", label="AUC = "+str(roc_auc))
				plt.legend(loc=4)
				plt.title("ROC curve plot", fontsize=14, verticalalignment="top", horizontalalignment="center")
				plt.xlabel("False positive rate", labelpad=3, fontsize=12)
				plt.ylabel("True positive rate", labelpad=3, fontsize=12)
				#plt.show()
				print("Saving the ROC curve: " + filename_roc)
				fig.savefig(filename_roc)
		except ValueError:
			print("--- Data ERROR ---")
			print("Error:Classify::classify_with_split: Value Error!")
			pass
